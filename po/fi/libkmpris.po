# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
# Tommi Nieminen <translator@legisign.org>, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-09-15 01:54+0000\n"
"PO-Revision-Date: 2023-09-26 18:15+0300\n"
"Last-Translator: Tommi Nieminen <translator@legisign.org>\n"
"Language-Team: Finnish <kde-i18n-doc@kde.org>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.12.3\n"

#: multiplexermodel.cpp:71
#, kde-format
msgctxt "@action:button"
msgid "Choose player automatically"
msgstr "Valitse pelaaja automaattisesti"
