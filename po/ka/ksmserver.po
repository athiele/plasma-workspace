# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-12-01 01:39+0000\n"
"PO-Revision-Date: 2023-11-05 06:07+0100\n"
"Last-Translator: Temuri Doghonadze <temuri.doghonadze@gmail.com>\n"
"Language-Team: Georgian <kde-i18n-doc@kde.org>\n"
"Language: ka\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 3.3.2\n"

#: logout.cpp:273
#, kde-format
msgid "Logout canceled by '%1'"
msgstr "გასვლა გაუქმდა %1-ის მიერ"

#: main.cpp:67
#, kde-format
msgid "$HOME not set!"
msgstr "$HOME დაყენებული არაა!"

#: main.cpp:71 main.cpp:80
#, kde-format
msgid "$HOME directory (%1) does not exist."
msgstr "$HOME საქაღალდე (%1) არ არსებობს."

#: main.cpp:74
#, kde-kuit-format
msgctxt "@info"
msgid ""
"No write access to $HOME directory (%1). If this is intentional, set "
"<envar>KDE_HOME_READONLY=1</envar> in your environment."
msgstr ""
"$HOME საქაღალდეში (%1) ჩაწერის უფლებების გარეშე. თუ ეს ძალით ქენით, თქვენს "
"გარემოში დააყენეთ ცვლადი <envar>kDE_HOME_READONLY=1</envar>."

#: main.cpp:82
#, kde-format
msgid "No read access to $HOME directory (%1)."
msgstr "$HOME საქაღალდიდან (%1) წაკითხვის უფლების გარეშე."

#: main.cpp:87
#, kde-format
msgid "$HOME directory (%1) is out of disk space."
msgstr "$HOME საქაღალდეში (%1) თავისუფალი ადგილი აღარაა."

#: main.cpp:90
#, kde-format
msgid "Writing to the $HOME directory (%2) failed with the error '%1'"
msgstr "$HOME საქაღალდეში (%2) ჩაწერა %1 შეცდომით დასრულდა"

#: main.cpp:104 main.cpp:143
#, kde-format
msgid "No write access to '%1'."
msgstr "ჩაწერის წვდომის გარეშე: '%1'."

#: main.cpp:106 main.cpp:145
#, kde-format
msgid "No read access to '%1'."
msgstr "წაკითხვის წვდომის გარეშე: '%1'."

#: main.cpp:116 main.cpp:130
#, kde-format
msgid "Temp directory (%1) is out of disk space."
msgstr "დროებით საქაღალდეში (%1) თავისუფალი ადგილი აღარაა."

#: main.cpp:119 main.cpp:133
#, kde-format
msgid ""
"Writing to the temp directory (%2) failed with\n"
"    the error '%1'"
msgstr ""
"დროებით საქაღალდეში (%2) ჩაწერა შემდეგი\n"
"   შეცდომით დასრულდა: %1"

#: main.cpp:150
#, kde-format
msgid ""
"The following installation problem was detected\n"
"while trying to start Plasma:"
msgstr ""
"Plasma-ის გაშვებისას ნაპოვნია\n"
"დაყენების შემდეგი პრობლემა:"

#: main.cpp:153
#, kde-format
msgid ""
"\n"
"\n"
"Plasma is unable to start.\n"
msgstr ""
"\n"
"\n"
"Plasma-ის გაშვების შეცდომა.\n"

#: main.cpp:160
#, kde-format
msgid "Plasma Workspace installation problem!"
msgstr "Plasma -ის დაყენების პრობლემა გაქვთ!"

#: main.cpp:194
#, kde-format
msgid ""
"The reliable Plasma session manager that talks the standard X11R6 \n"
"session management protocol (XSMP)."
msgstr ""
"საიმედო Plasma-ის სესიების მმართველი, რომელიც სტანდარტულ X11R6-ის\n"
"სესიის მართვის პროტოკოლით (XSMP) ლაპარაკობს."

#: main.cpp:198
#, kde-format
msgid "Restores the saved user session if available"
msgstr "შენახული მომხმარებლის სესიის აღდგენა, თუ ის ხელმისაწვდომია"

#: main.cpp:201
#, kde-format
msgid "Also allow remote connections"
msgstr "დაშორებული მიერთებების დაშვება"

#: main.cpp:204
#, kde-format
msgid "Starts the session in locked mode"
msgstr "სესიის დაბლოკილ რეჟიმში გაშვება"

#: main.cpp:208
#, kde-format
msgid ""
"Starts without lock screen support. Only needed if other component provides "
"the lock screen."
msgstr ""
"ეკრანის დაბლოკვის მხარდაჭერის გარეშე გაშვება. საჭიროა მხოლოდ აშინ, თუ ეკრანს "
"სხვა კომპონენტი ბლოკავს."

#: server.cpp:813
#, kde-format
msgctxt "@label an unknown executable is using resources"
msgid "[unknown]"
msgstr "[უცნობი]"

#: server.cpp:836
#, kde-kuit-format
msgctxt "@label notification; %1 is a list of executables"
msgid ""
"Unable to manage some apps because the system's session management resources "
"are exhausted. Here are the top three consumers of session resources:\n"
"%1"
msgstr ""
"ზოგერთი აპის მართვა შეუძლია, რადგან სისტემის სესიის მართვის რესურსები "
"ამოიწურა. იხილეთ ჩამონათვალი 3 ყველაზე მეტი რესურსის მხარჯველის:\n"
"%1"

#: server.cpp:1108
#, kde-kuit-format
msgctxt "@label notification; %1 is an executable name"
msgid ""
"Unable to restore <application>%1</application> because it is broken and has "
"exhausted the system's session restoration resources. Please report this to "
"the app's developers."
msgstr ""
"<application>%1</application> ვერ აღვადგინე, რადგან ის დაზიანებულია და "
"გააჩნია ამოწურული სისტემის სესიის აღდგენის რესურსები. მისწერეთ ამის შესახებ "
"აპის ავტორებს."

#~ msgid "Session Management"
#~ msgstr "სესიების მართვა"

#~ msgid "Log Out"
#~ msgstr "გასვლა"

#~ msgid "Shut Down"
#~ msgstr "გამორთვა"

#~ msgid "Reboot"
#~ msgstr "გადატვირთვა"

#~ msgid "Log Out Without Confirmation"
#~ msgstr "გასვლა დადასტურების გარეშე"

#~ msgid "Shut Down Without Confirmation"
#~ msgstr "გამორთვა დადასტურების გარეშე"

#~ msgid "Reboot Without Confirmation"
#~ msgstr "გადატვირთვა დადასტურების გარეშე"

#~ msgid "No write access to $HOME directory (%1)."
#~ msgstr "$HOME საქაღალდეში (%1) ჩაწერის უფლების გარეშე."
